# AntiRSI

AntiRSI is a program to help prevent and alleviate Repetitive Strain Injury
by enforcing breaks from your computer.

## Bulding (macOS)

    xcodebuild

## Building (GNUstep)

Building AntiRSI for GNUstep requires [buildtool](https://github.com/gnustep/tools-buildtool).

    buildtool


## License

AntiRSI is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
 
AntiRSI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with AntiRSI; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
